import { Avatar } from '@material-ui/core';
import { StopRounded } from '@material-ui/icons';
import React from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router';
import ReactTimeago from 'react-timeago';
import '../css/Chat.css';
import { selectImage } from '../features/appSlice';
import { db } from '../firebase';

const Chat = ({  id, username, timestamp, imageUrl, read, profilePic }) => {

    const dispatch = useDispatch();
    //get user history navigation from router
    const history = useHistory();

    const openChat = () =>{

        if(!read){
            //set url to selected image
            dispatch(selectImage(imageUrl));
            //update read in db
            db.collection('posts').doc(id).set(
                {
                    read: true,
                }, { merge: true}
            );
           
        }

         //navigate the user after chat clicked
         history.push("/chats/view");
    }



    return (
        <div onClick={openChat} className="chat">
            <Avatar className="chat__avatar" src={profilePic} />
            <div className="chat__info">
                <h4>{username}</h4>
                <p>
                { !read && "Tap to view -"}{" "} 
                
                <ReactTimeago date={new Date(timestamp?.toDate()).toUTCString()} /></p>
            </div>


            {!read && <StopRounded className="chat__readIcon" />}
        </div>
    )
}

export default Chat
