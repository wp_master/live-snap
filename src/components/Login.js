import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import '../css/Login.css';
import AddAPhotoIcon from '@material-ui/icons/AddAPhoto';
import { Button } from '@material-ui/core';
import { auth, provider } from '../firebase';
import { login, logout } from '../features/appSlice';

const Login = () => {

    const dispatch = useDispatch();

    //check on load if user is logged in an authenticated
    useEffect(() => {
        auth.onAuthStateChanged( (authUser)=>{
            if(authUser){
                dispatch(login({
                    username: authUser.displayName,
                    profilePic: authUser.photoURL,
                    id: authUser.uid,
                }))
            }else{
                dispatch(logout())
            }
        } )
    }, [])

    //sign in user with Gmail
    const signIn = () =>{
        auth.signInWithPopup(provider).then(
            //dispatch result with user details
            result => {
                dispatch(login({
                    username: result.user.displayName,
                    profilePic: result.user.photoURL,
                    id: result.user.uid,
                }))
            }
        ).catch(error=>{
            //display error if login failed
            alert(error.message);
        })
    };


    return (
        <div className="login">
            <div className="login_container">
                <h1 className="login__title">LiveSnap</h1>
                <AddAPhotoIcon className="login__icon" />
                <Button variant='outlined' onClick={signIn}>Sign in</Button>
            </div>
        </div>
    )
}

export default Login
