import React, {useCallback, useRef, useState} from 'react';
import Webcam from 'react-webcam';
import RadioButtonUncheckedIcon from '@material-ui/icons/RadioButtonUnchecked';
import { useDispatch } from 'react-redux';
import { setCameraImage } from '../features/cameraSlice';
import { useHistory } from 'react-router';
import '../css/WebcamCapture.css';

const videoConstraints = {
    width:250,
    height:400,
    facingMode: "user",
};

const WebcamCapture = () => {
    //use reference
    const webcamRef = useRef(null);

   const dispatch = useDispatch();
   //get user navigation history from router
   const history = useHistory();
 

    const captureShot = useCallback(() => {
        //take snapshot image
        const imageSrc = webcamRef.current.getScreenshot(); 
        //dispatch the camera image action
        dispatch(setCameraImage(imageSrc));

        //navigate the user after getting snapshot
        history.push('/preview');

        

            
    },[webcamRef]);


    return (
        <div className="webcamCapture"> 
            <Webcam 
                audio={false}
                height={videoConstraints.height}
                ref={webcamRef}
                screenshotFormat="image/jpeg"
                width={videoConstraints.width}
                videoConstraints={videoConstraints}

            />

            <RadioButtonUncheckedIcon 
                className="webcamCapture__button"
                fontSize="large"
                onClick={captureShot}

            /> 

        </div>
    )
};




export default WebcamCapture;
