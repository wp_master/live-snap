import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import '../css/ChatView.css';
import { selectSelectedImage } from '../features/appSlice';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { CountdownCircleTimer } from 'react-countdown-circle-timer';


const ChatView = () => {

    const selectedImage = useSelector(selectSelectedImage);
    const history = useHistory();

    useEffect(() => {
        //exit screen if no image selected
        if(!selectedImage){
            
            exitScreen();
        }


    }, [selectedImage])

    const exitScreen = () => {
        //navigate user back to chats screen
        history.replace('/chats');
    };

    return (
        <div className="chatView">
            <ArrowBackIcon onClick={exitScreen} className="exitScreen"/>
            <img src={selectedImage} alt="" />
            <div className="chatView__timer">
                <CountdownCircleTimer
                    isPlaying
                    duration={10}
                    strokeWidth={6}
                    size={50}
                    colors={
                    [
                        ["#004777", 0.33],
                        ["#F7BB01", 0.33],
                        ["#A30000", 0.33],
                    ]
                    }

                >

                    {
                        ({remainingTime}) => {

                            if(remainingTime === 0){
                                //navigate when timer is ended
                                exitScreen();
                            }
                            return remainingTime;
                        }
                    }
                </CountdownCircleTimer>
            </div>
            
        </div>
    )
}

export default ChatView
