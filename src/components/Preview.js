import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import '../css/Preview.css';
import { resetCameraImage, selectCameraImage } from '../features/cameraSlice';
import CloseIcon from '@material-ui/icons/Close';
import TextFieldsIcon from '@material-ui/icons/TextFields';
import { AttachFile, Create, Crop, MusicNote, Note, Send, Timer } from '@material-ui/icons';
import { v4 as uuid } from "uuid";
import { db, storage } from "../firebase";
import firebase from 'firebase';
import { selectUser } from '../features/appSlice';

const Preview = () => {
    //get the selected image from data layer
    const cameraImage = useSelector(selectCameraImage);
    const history = useHistory();
    const dispatch = useDispatch();
    //get user data
    const user = useSelector(selectUser);

    

    useEffect( ()=>{
        console.log(user);
        if(!cameraImage) {
            //redirect user to home if thre is no image captured
            history.replace('/');
        }

    },[cameraImage, history] );

    //close preview function
    const closePreview = () => {
        dispatch(resetCameraImage());
    };

    const sendPost = () => {
        const id = uuid();

        const uploadTask = storage.ref(`posts/${id}`)
        .putString(cameraImage, "data_url");

        uploadTask.on('state_changed', null,(error)=>{
            //console error
            console.log(error)
        },
        () => {
            storage.ref('posts').child(id)
            .getDownloadURL()
            .then(
                (url) => {
                    db.collection('posts').add({
                        imageUrl: url,
                        username: user.username,
                        read: false,
                        profilePic:user.profilePic,
                        timestamp:firebase.firestore.FieldValue.serverTimestamp(),
                    })
                    history.replace('/chats');
                }

            );
        }
        );
    };

    return (
        <div className="preview">
            <CloseIcon onClick={closePreview} className="preview__close"/>
            <div className="preview__toolbarRight">
                <TextFieldsIcon />
                <Create />
                <Note />
                <MusicNote />
                <AttachFile />
                <Crop />
                <Timer />
            </div>
            <img className="preview__capturedImage"  src={cameraImage} alt="" />
            <div onClick={sendPost} className="preview__footer">
                <h2>Send Now</h2>
                <Send  className="preview__sendIcon"/>
            </div>
        </div>
    )
}

export default Preview
