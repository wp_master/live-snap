import { Avatar } from '@material-ui/core';
import { ChatBubble, Search } from '@material-ui/icons';
import React, { useEffect, useState } from 'react';
import '../css/Chats.css';
import { auth, db } from '../firebase';
import Chat from '../components/Chat';
import { useDispatch, useSelector } from 'react-redux';
import { selectUser } from '../features/appSlice';
import RadioButtonUncheckedIcon from '@material-ui/icons/RadioButtonUnchecked';
import { useHistory } from 'react-router';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import { resetCameraImage } from '../features/cameraSlice';


const Chats = () => {
    //posts state
    const [posts, setPosts] = useState([]);
    //get user data
    const user = useSelector(selectUser);
    const dispatch = useDispatch();
    const history = useHistory();

    //get updated posts on load
    useEffect(() => {
        
        db.collection('posts').orderBy('timestamp', 'desc').onSnapshot(
            snapshot=>{
                setPosts(snapshot.docs.map(doc=>(
                    {
                        id:doc.id,
                        data: doc.data(),
                    }
                )))
            }
            )

    }, [])

    const takeSnap = () => {
        dispatch(resetCameraImage);
        //navigate user to snap screen
        history.push("/");
    };



    return (
        <div className="chats">
            <div className="chats__header">
                <Avatar src={user.profilePic} onClick={ ()=> auth.signOut() } className="chats__avatar" />
                <div className="chats__search">
                    <Search className="chats__searchIcon"/>
                    <input type="text" placeholder="Friends" />
                    <ChatBubble className="chats__chatIcon" />
                    <ExitToAppIcon className="chats__exit" onClick={ ()=> auth.signOut() } />
                </div>
            </div>
            <div className="chats__posts">
             
            { posts.map( ({id, data: {profilePic, username, timestamp, imageUrl, read}})=> (
                <Chat 
                    key={id}
                    id={id}
                    username={username}
                    timestamp={timestamp}
                    imageUrl={imageUrl}
                    read={read}
                    profilePic={profilePic}


                />
            ) ) }


            </div>

            <RadioButtonUncheckedIcon 
            className="chats__takPicIcon"
            onClick={takeSnap}
            fontSize='large'


            />
        </div>
    )
}

export default Chats
