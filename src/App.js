import React from "react";
import "./App.css";
import WebcamCapture from "./components/WebcamCapture";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Preview from "./components/Preview";
import Chats from "./components/Chats";
import ChatView from './components/ChatView';
import { selectUser } from "./features/appSlice";
import { useDispatch, useSelector } from "react-redux";
import Login from "./components/Login";
import AddAPhotoIcon from '@material-ui/icons/AddAPhoto';


function App() {

  const user = useSelector(selectUser);
  const dispatch = useDispatch();

  return (
    <div className="app">
      <Router>
        
        
          {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
            

            

            {/* check if user logged in */ }
            {!user ? (
              <Login />
            ): (
              <>
              <h2 className="app__title">LiveSnap</h2>
                <AddAPhotoIcon className="app__icon" />
              <div className="app__body">

              <div className="app__bodyBackground">
               
                <Switch>
                  <Route exact path="/">
                    <WebcamCapture />
                  </Route>
                  <Route exact path="/preview">
                    <Preview />
                  </Route>
                  <Route exact path="/chats/view">
                    <ChatView />
                  </Route>
                  <Route exact path="/chats">
                    <Chats />
                  </Route>
                </Switch>
              </div>
              </div>
      
              </>
            )}

      </Router>              
    </div>
  );
}

export default App;
