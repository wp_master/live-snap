import firebase from 'firebase';

const firebaseConfig = {
    apiKey: "AIzaSyBRY_dhl9-KgP1NmdtWiLWh9Av4ftMc6fM",
    authDomain: "live-snap.firebaseapp.com",
    projectId: "live-snap",
    storageBucket: "live-snap.appspot.com",
    messagingSenderId: "1009292751722",
    appId: "1:1009292751722:web:f7aa5c7a1d8d163f3deb13"
};

//initialize firebase
const firebaseApp = firebase.initializeApp(firebaseConfig);
const db = firebaseApp.firestore();
const auth = firebase.auth();
const storage = firebase.storage();
const provider = new firebase.auth.GoogleAuthProvider();

export { db, auth, storage, provider };

